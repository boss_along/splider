package com.alibaba.splider.thread;

public interface ICallableTask {

    Object execute() throws Exception ;
}
