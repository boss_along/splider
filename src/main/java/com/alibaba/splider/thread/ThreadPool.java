package com.alibaba.splider.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Auther: boss_along
 * @Date: 2018/12/20 18:48
 * @Description:
 */
public class ThreadPool {

    private static ExecutorService executorService = null;

    private ThreadPool() {}

    public static ExecutorService newInstance(){
        if(executorService==null){
            synchronized (ThreadPool.class){
                if(executorService==null){
                    executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
                }
            }
        }
        return executorService;
    }
}
