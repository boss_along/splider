package com.alibaba.splider.thread;

import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * @Auther: boss_along
 * @Date: 2018/12/21 20:03
 * @Description:
 */
public class CallableTask implements Callable {

    private List<DefaultTransactionDefinition> list;

    private ICallableTask callableTask;

    public CallableTask(List<DefaultTransactionDefinition> list, ICallableTask callableTask) {
        this.list = list;
        this.callableTask = callableTask;
    }

    @Override
    public Object call() throws Exception {
        return callableTask.execute();
    }
}
