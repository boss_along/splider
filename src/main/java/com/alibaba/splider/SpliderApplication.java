package com.alibaba.splider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.alibaba.splider.mapper","com.alibaba.splider.common.mapper"})
public class SpliderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpliderApplication.class, args);
    }

}

