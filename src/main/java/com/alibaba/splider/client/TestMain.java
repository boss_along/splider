package com.alibaba.splider.client;

import org.springframework.beans.BeanUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.InvocationTargetException;

/**
 * @Auther: boss_along
 * @Date: 2018/12/14 12:10
 * @Description:
 */
public class TestMain {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
     /*   Client client = ClientBuilder.newClient();
        WebTarget target = client.target("www.baidu.com");

        WebTarget path = target.path("a").path("b");
        Form form = new Form();
        form.param("name","zhangsan");

        path.request(MediaType.APPLICATION_JSON_PATCH_JSON_TYPE).post(Entity.entity(form, MediaType.APPLICATION_JSON_TYPE), String.class);
*/
        Stu a = new Stu();
        a.setT(1);
        Stu b = new Stu();
        long ll = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            org.apache.commons.beanutils.BeanUtils.copyProperties(b,a);
        }
        long lll = System.currentTimeMillis();
        System.out.println(lll-ll);
        long llla = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            BeanUtils.copyProperties(a,b);
        }
        long llll = System.currentTimeMillis();

        System.out.println(llll-llla);

    }
}
class Stu{
    private Integer t;

    public Integer getT() {
        return t;
    }

    public void setT(Integer t) {
        this.t = t;
    }
}
