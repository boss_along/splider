package com.alibaba.splider.client;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Auther: boss_along
 * @Date: 2018/12/13 17:30
 * @Description:
 */
public class ProxyIPClient {

    public static void main(String[] args) {
        //HttpClients
        CloseableHttpClient httpClient = HttpClients.createDefault();
        List<HttpGet> httpGetList = new ArrayList<>(2605);
        for (int i = 1; i < 2606; i++) {
            httpGetList.add(new HttpGet("https://www.kuaidaili.com/free/inha/"+i));
        }

        //HttpHost httpHost = new HttpHost("222.171.251.43", 40149);

        //RequestConfig build = RequestConfig.custom().setProxy(httpHost).setConnectTimeout(10000).setSocketTimeout(10000).setConnectionRequestTimeout(3000).build();
        //RequestConfig.custom().setConnectionRequestTimeout()
        //httpGet.setConfig(build);
        //设置请求头消息


        StringBuilder sb = new StringBuilder();
        httpGetList.forEach(httpGet ->{

            httpGet.setHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");

            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpGet);
                if(Objects.nonNull(response)){
                    HttpEntity entity = response.getEntity();
                    String s = EntityUtils.toString(entity, "UTF-8");
                    sb.append(s);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                    if (response != null) {
                        try {
                            response.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

            }


        });

        FileWriter fw = null;
        PrintWriter pw = null;
        try {
            fw = new FileWriter(new File("d:/html/a.text"));
            pw = new PrintWriter(fw);
            pw.write(sb.toString());
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {

                if(httpClient!=null){
                    httpClient.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(pw!=null){
                    pw.close();
                }

                if(fw!=null){
                    fw.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
