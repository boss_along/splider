package com.alibaba.splider.entity;

import java.io.Serializable;

/**
 * (TUserRole)实体类
 *
 * @author makejava
 * @since 2019-01-10 14:45:00
 */
public class TUserRole implements Serializable {
    private static final long serialVersionUID = -83031829204246180L;
    
    private Integer id;
    
    private Integer roleId;
    
    private Integer userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}