package com.alibaba.splider.entity;

import java.io.Serializable;

/**
 * (TRoleMenu)实体类
 *
 * @author makejava
 * @since 2019-01-10 14:44:59
 */
public class TRoleMenu implements Serializable {
    private static final long serialVersionUID = 303453248439686362L;
    
    private Integer id;
    
    private Integer menuId;
    
    private Integer roleId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}