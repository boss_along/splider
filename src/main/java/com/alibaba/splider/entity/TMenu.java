package com.alibaba.splider.entity;

import java.io.Serializable;

/**
 * (TMenu)实体类
 *
 * @author makejava
 * @since 2019-01-10 14:44:54
 */
public class TMenu implements Serializable {
    private static final long serialVersionUID = -85119698035293381L;
    
    private Integer id;
    
    private String icon;
    
    private String name;
    
    private Integer state;
    
    private String url;
    
    private Integer pId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPId() {
        return pId;
    }

    public void setPId(Integer pId) {
        this.pId = pId;
    }

}