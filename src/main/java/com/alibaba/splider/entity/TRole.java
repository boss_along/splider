package com.alibaba.splider.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * (TRole)实体类
 *
 * @author makejava
 * @since 2019-01-10 14:44:58
 */
public class TRole implements Serializable {
    private static final long serialVersionUID = 635785657658431677L;
    
    private Integer id;
    
    private String bz;
    
    private String name;
    
    private String remarks;

    private Set<TMenu> menus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Set<TMenu> getMenus() {
        return menus;
    }

    public void setMenus(Set<TMenu> menus) {
        this.menus = menus;
    }
}