package com.alibaba.splider.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * (TUser)实体类
 *
 * @author makejava
 * @since 2019-01-10 14:44:59
 */
public class TUser implements Serializable {
    private static final long serialVersionUID = -39112481135426269L;
    
    private Integer id;
    
    private String bz;
    
    private String password;
    
    private String trueName;
    
    private String userName;
    
    private String remarks;

    private Set<TRole> roles;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Set<TRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<TRole> roles) {
        this.roles = roles;
    }
}