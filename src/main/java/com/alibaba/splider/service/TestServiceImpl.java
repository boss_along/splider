package com.alibaba.splider.service;

import com.alibaba.splider.mapper.TeacherMapper;
import com.alibaba.splider.mapper.TestMapper;
import com.alibaba.splider.pojo.Teacher;
import com.alibaba.splider.pojo.Test;
import com.alibaba.splider.thread.ThreadPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.context.ContextLoader;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * @Auther: boss_along
 * @Date: 2018/12/20 18:53
 * @Description:
 */
@Service
public class TestServiceImpl implements com.alibaba.splider.service.TestService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private TestMapper testMapper;

    @Transactional
    @Override
    public void test() throws Exception {
        FutureTask<Object> future1 = new FutureTask<>(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                try{
                    Teacher a = new Teacher();
                    a.setTname("李四");
                    teacherMapper.insert(a);
                }catch (Exception e){
                    e.printStackTrace();
                    throw e;
                }
                return null;
            }
        });

        FutureTask<Object> future2 = new FutureTask<>(new Callable<Object>() {
            @Override
            public Object call() throws Exception {

                // spring无法处理thread的事务，声明式事务无效
                 DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                 def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                 PlatformTransactionManager txManager = ContextLoader.getCurrentWebApplicationContext().getBean(PlatformTransactionManager.class);
                 TransactionStatus status = txManager.getTransaction(def);
                try{
                    Test a = new Test();
                    a.setName("张三");
                    int c = 1/0;
                    testMapper.insert(a);
                }catch (Exception e){
                    e.printStackTrace();
                    throw e;
                }

                return null;
            }
        });


        ThreadPool.newInstance().execute(future1);
        ThreadPool.newInstance().execute(future2);
        try {
            Object o = future1.get();
            Object o2 = future2.get();
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }

    }
}
