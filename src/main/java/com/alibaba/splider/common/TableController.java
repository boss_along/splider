package com.alibaba.splider.common;

import com.alibaba.splider.common.service.TablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Auther: boss_along
 * @Date: 2018/12/28 13:46
 * @Description:
 */
@RestController
public class TableController {

    @Autowired
    private TablesService tablesService;

    @RequestMapping("/table")
    public List<Map> listTable(){
       return tablesService.listTable();
    }

    @RequestMapping("/column/{tableName}")
    public List<Map> listTableColumn(@PathVariable("tableName") String tableName){
        return tablesService.listTableColumn(tableName);
    }
}
