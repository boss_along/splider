package com.alibaba.splider.common.service;

import java.util.List;
import java.util.Map;

public interface TablesService {

    List<Map> listTable();

    List<Map> listTableColumn(String tableName);
}
