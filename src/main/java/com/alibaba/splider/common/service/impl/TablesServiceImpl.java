package com.alibaba.splider.common.service.impl;

import com.alibaba.splider.common.mapper.TablesMapper;
import com.alibaba.splider.common.service.TablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther: boss_along
 * @Date: 2018/12/28 09:57
 * @Description:
 */
@Service
public class TablesServiceImpl implements TablesService {

    @Autowired
    private TablesMapper tablesMapper;

    @Override
    public List<Map> listTable() {
        return tablesMapper.listTable();
    }

    @Override
    public List<Map> listTableColumn(String tableName) {
        return tablesMapper.listTableColumn(tableName);
    }
}
