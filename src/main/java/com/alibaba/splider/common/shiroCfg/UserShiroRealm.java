package com.alibaba.splider.common.shiroCfg;

import com.alibaba.splider.entity.TUser;
import com.alibaba.splider.service.TRoleService;
import com.alibaba.splider.service.TUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.ws.soap.Addressing;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: boss_along
 * @Date: 2019/1/10 11:23
 * @Description:
 */
public class UserShiroRealm extends AuthorizingRealm {

    @Autowired
    private TUserService tUserService;

    private TRoleService tRoleService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        Integer primaryPrincipal = (Integer) principalCollection.getPrimaryPrincipal();

        TUser tUser = tUserService.queryById(primaryPrincipal);
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        //tRoleService.

        simpleAuthorizationInfo.addRole("0001");
        simpleAuthorizationInfo.addStringPermission("0001");

        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //加这一步的目的是在Post请求的时候会先进认证，然后在到请求
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        //获取用户信息
        String uname = authenticationToken.getPrincipal().toString();
        //String uname = user.get("username");
        //String pass = user.get("passpassword");
        if (uname == null) {
            //这里返回后会报出对应异常
            return null;
        } else {
            //这里验证authenticationToken和simpleAuthenticationInfo的信息

            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(uname, "", getName());
            return simpleAuthenticationInfo;
        }
    }
}
