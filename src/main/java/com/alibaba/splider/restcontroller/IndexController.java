package com.alibaba.splider.restcontroller;

import com.alibaba.splider.pojo.Test;
import com.alibaba.splider.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * @Auther: boss_along
 * @Date: 2018/12/14 13:09
 * @Description:
 */
@RestController
public class IndexController {

    public static void main(String[] args) {
        List<Double> list = new ArrayList<>();
        list.add(0D);
        list.add(5D);
        list.add(3D);
        list.add(1D);
        list.add(3D);
        BigDecimal bigDecimal1 = BigDecimal.valueOf(0.0D);
        BigDecimal bigDecimal2 = BigDecimal.valueOf(10D);
        list.forEach(h -> {
            bigDecimal1.add(bigDecimal2);
            //System.out.println(addadd.doubleValue());
        });
        BigDecimal addadd = bigDecimal1.add(bigDecimal2);
        BigDecimal divide = bigDecimal2.divide(bigDecimal1,6,  RoundingMode.HALF_UP);
        BigDecimal multiply = divide.multiply(BigDecimal.valueOf(5D)).setScale(4,RoundingMode.CEILING);
        BigDecimal[] bigDecimals = bigDecimal2.divideAndRemainder(bigDecimal1);
        System.out.println();

        List<Double> list1 = new ArrayList<>();
        list.add(0D);
        list.add(5D);
        list.add(3D);
        list.add(1D);
        list.add(3D);

        Collections.sort(list, new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {

                return o1.compareTo(o2);
            }
        });
        System.out.println("");
    }

    @Autowired
    private TestService testService;

    @RequestMapping("/")
    public String index(){
try{
    testService.test();
}catch (Exception e){
    e.printStackTrace();
}

        return "nihao";
    }
}
