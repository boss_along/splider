package com.alibaba.splider.controller;

import com.alibaba.splider.entity.TRoleMenu;
import com.alibaba.splider.service.TRoleMenuService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (TRoleMenu)表控制层
 *
 * @author makejava
 * @since 2019-01-10 14:44:59
 */
@RestController
@RequestMapping("tRoleMenu")
public class TRoleMenuController {
    /**
     * 服务对象
     */
    @Resource
    private TRoleMenuService tRoleMenuService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TRoleMenu selectOne(Integer id) {
        return this.tRoleMenuService.queryById(id);
    }

}