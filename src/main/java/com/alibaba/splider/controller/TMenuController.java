package com.alibaba.splider.controller;

import com.alibaba.splider.entity.TMenu;
import com.alibaba.splider.service.TMenuService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (TMenu)表控制层
 *
 * @author makejava
 * @since 2019-01-10 14:44:57
 */
@RestController
@RequestMapping("tMenu")
public class TMenuController {
    /**
     * 服务对象
     */
    @Resource
    private TMenuService tMenuService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public TMenu selectOne(Integer id) {
        return this.tMenuService.queryById(id);
    }

}